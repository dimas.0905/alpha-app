# Update 02/07/2021
Apologize for being late on the submission, I have been busy for these past 2 days due to my workloads and swab test appointment. Also I am not finished with the pseudocodes that you asked. Going to finish it but expect a little delay. In the meantime you can check my proposed design on the provided pdf. Thank you!

```
docker-compose up
```

# Introduction
A simple web app to query SSH login attempts based on Docker environment, that consists of 1 server container and 3 client containers.
Every time someone attempts to SSH one of the client containers, it will be recorded as a log on the client container and then the log will be passed to the server container where later the server container will show the log via web.

# Server Container
Built in a simple bash script that concatenated the logs from client containers collected from a mounted volume (that is also mounted on client containers) every 1 minute via cron, and Node.js code using Express node module as the web server to display the final output. In this case, I'm using another Ubuntu container and then run curl http://[servercontaineraddress]:8080/output.txt

# Client Container
The client container(s) collects SSH auth logs using rsyslog, parses it in regex expression, get the total count of success/failed attempts, and save the result on a mounted volume every 1 minute via cron.

# Few Things to Note
I'm using a network_mode: "bridge" on the docker-compose.yml file so that I'm enable to do SSH/curl from another Ubuntu container that I created manually using default config.
SSH username and password is hard-coded on the Dockerfile-Client (username: sshtest, password: test1234)