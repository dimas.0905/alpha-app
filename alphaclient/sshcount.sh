# create env variable for hostname
HOSTNAME=$(cat /etc/hostname)

# parse auth log using regex expr and save it on a txt file
echo "$HOSTNAME has $(cat /var/log/auth.log | egrep -c '(: Failed.*for ([a-zA-Z]*).*)|(: Accepted.*for([a-zA-Z]*).*)') SSH attempt(s)" | tee /output/test-$HOSTNAME.txt
