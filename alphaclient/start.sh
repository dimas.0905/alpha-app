# start services
sleep 2 && \
/etc/init.d/rsyslog start && \
/etc/init.d/ssh start && \
/etc/init.d/cron start && \

./sshcount.sh

# add cron job for parsing script
sched="*/1 * * * * /script/sshcount.sh"
(crontab -u root -l; echo "$sched" ) | crontab -u root -
tail -f /dev/null
